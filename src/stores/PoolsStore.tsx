import { createContext, useCallback, useContext, useState } from 'react'
import { Pool } from '@/types'
import { CHAINS_SUPPORTED_BY_NAME } from '@/constants'

interface PoolsStoreState {
  pools: Pool[]
  poolSelected?: Pool
  selectPool: (id: string) => void
}

const PoolsStore = createContext<PoolsStoreState>({} as PoolsStoreState)

const mockPools: Pool[] = [
  {
    id: '1',
    address: { [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0x' },
    name: 'GigaPool X',
    available: 71_000_000,
    apy: 11,
    term: 'Rolling',
    withdrawal: '30 days, auctions',
    co2: '12B Mtons',
    watt: '12,4 GW',
  },
  {
    id: '2',
    address: { [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0xa2978b6b29764c1d9821bad52fea23f100ade10f' },
    name: 'GigaPool Senior',
    available: 215_000,
    apy: 4.2,
    term: 'Rolling',
    withdrawal: '30 days, auctions',
    co2: '12B Mtons',
    watt: '12,4 GW',
  },
  {
    id: '3',
    address: { [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0x' },
    name: 'GigaPool X',
    available: 71_000_000,
    apy: 11,
    term: 'Rolling',
    withdrawal: '30 days, auctions',
    co2: '12B Mtons',
    watt: '12,4 GW',
  },
  {
    id: '4',
    address: { [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0x' },
    name: 'GigaPool X',
    available: 71_000_000,
    apy: 11,
    term: 'Rolling',
    withdrawal: '30 days, auctions',
    co2: '12B Mtons',
    watt: '12,4 GW',
  },
  {
    id: '5',
    address: { [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0x' },
    name: 'GigaPool X',
    available: 71_000_000,
    apy: 11,
    term: 'Rolling',
    withdrawal: '30 days, auctions',
    co2: '12B Mtons',
    watt: '12,4 GW',
  },
  {
    id: '6',
    address: { [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0x' },
    name: 'GigaPool X',
    available: 71_000_000,
    apy: 11,
    term: 'Rolling',
    withdrawal: '30 days, auctions',
    co2: '12B Mtons',
    watt: '12,4 GW',
  },
]

export const PoolsStoreProvider = ({ children }: React.PropsWithChildren) => {
  const [pools] = useState<Pool[]>(mockPools)
  const [poolSelected, setPoolSelected] = useState<Pool | undefined>()
  const selectPool = useCallback(
    (id: string) => setPoolSelected(pools.find((pool) => pool.id === id)),
    [setPoolSelected]
  )

  const state: PoolsStoreState = {
    pools,
    poolSelected,
    selectPool,
  }

  return <PoolsStore.Provider value={state}>{children}</PoolsStore.Provider>
}

export const usePoolsStore = () => useContext(PoolsStore)
