import { MainPageStatsChart, MainPageStatsTable } from '@/components'
import { Flex } from '@chakra-ui/react'

export const MainPageStats = () => (
  <Flex
    flexDir={{ sm: 'column', md: 'row' }}
    justifyContent={'space-between'}
    gap={{ sm: '30px', xl: '40px', xxl: '60px' }}
    px={{ sm: '15px', md: '24px', xxl: 0 }}
  >
    <MainPageStatsChart />
    <MainPageStatsTable />
  </Flex>
)
