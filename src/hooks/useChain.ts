import { DEFAULT_CHAIN } from '@/constants'
import { useNetwork, useSwitchNetwork } from 'wagmi'

export const useChain = () => {
  const { chain: activeChain } = useNetwork()
  const chain = !activeChain || activeChain?.unsupported ? DEFAULT_CHAIN : activeChain
  const { switchNetwork } = useSwitchNetwork()
  return { activeChain, chain, switchNetwork } as const
}
