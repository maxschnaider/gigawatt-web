import { Button, ConnectButton, DepositInput, DepositSummary, Modal } from '@/components'
import { useDepositStore } from '@/stores'
import { DepositUIState } from '@/types'
import { Stack } from '@chakra-ui/react'

export const DepositModal = () => {
  const { isOpenDepositModal, closeDepositModal, state, approve, deposit } = useDepositStore()

  const SwitchNetworkButton = () => <ConnectButton w={'full'} h={'60px'} />
  const ApproveButton = () => (
    <Button onClick={() => approve()} isLoading={state === DepositUIState.Loading}>
      Approve USDC
    </Button>
  )
  const DepositButton = () => (
    <Button
      onClick={() => deposit()}
      isLoading={state === DepositUIState.Loading}
      isDisabled={state !== DepositUIState.ReadyToDeposit}
    >
      Deposit
    </Button>
  )
  const ActiveButton = () => {
    if (state === DepositUIState.WrongNetwork) return <SwitchNetworkButton />
    if (state === DepositUIState.ExceedsAllowance) return <ApproveButton />
    return <DepositButton />
  }

  return (
    <Modal title={'Deposit'} isOpen={isOpenDepositModal} onClose={closeDepositModal}>
      <Stack spacing={4}>
        <DepositSummary />
        <DepositInput />
        <ActiveButton />
      </Stack>
    </Modal>
  )
}
