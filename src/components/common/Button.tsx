import { ButtonProps, Button as ChakraButton } from '@chakra-ui/react'

export type { ButtonProps }

export const Button = ({ children, ...props }: ButtonProps) => (
  <ChakraButton
    h={'60px'}
    // w={'full'}
    colorScheme='blackAlpha'
    bg={'black'}
    color={'white'}
    borderRadius={'full'}
    fontSize={16}
    gap={2}
    alignItems={'center'}
    fontWeight={'normal'}
    _hover={{ bg: 'blackAlpha.800' }}
    {...props}
  >
    {children}
  </ChakraButton>
)
