'use client'

import { Flex, Image } from '@chakra-ui/react'

export const Loader = () => (
  <Flex w={'full'} h={'100vh'} alignItems={'center'} justifyContent={'center'}>
    <Image src='/assets/images/gigawatt-logo.svg' h={'30px'} opacity={0.2} />
  </Flex>
)
