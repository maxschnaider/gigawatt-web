export type Address = `0x${string}`

export type Pool = {
  id: string
  address: { [chainId: number]: Address }
  name: string
  available: number
  apy?: number
  term?: string
  withdrawal?: string
  co2?: string
  watt?: number | string
}

export type Token = {
  address: { [chainId: number]: Address }
  symbol: string
  decimals: number
  name?: string
  imageURI?: string
}

export enum DepositUIState {
  WalletNotConnected,
  WrongNetwork,
  InvalidAmount,
  ExceedsAllowance,
  ReadyToDeposit,
  DepositSubmitted,
  Loading,
  Idle,
}
