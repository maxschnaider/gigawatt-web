import { Flex, FlexProps } from '@chakra-ui/react'

export const Container = ({ children, ...props }: FlexProps) => (
  <Flex
    p={{ sm: '12px 12px 8px', md: '20px 20px 10px' }}
    bg={'bg.gray'}
    border={'1px solid'}
    borderColor={'border.gray'}
    borderRadius={'12px'}
    {...props}
  >
    {children}
  </Flex>
)
