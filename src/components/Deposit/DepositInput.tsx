import { Button, Input } from '@/components'
import { useDepositStore } from '@/stores'
import { DepositUIState } from '@/types'
import { Flex } from '@chakra-ui/react'

export const DepositInput = () => {
  const { amount, setAmount, maxAmount, state } = useDepositStore()

  const isInvalidAmount = amount && state === DepositUIState.InvalidAmount

  return (
    <Flex pos={'relative'}>
      <Input
        placeholder='42,000'
        type={'number'}
        borderColor={isInvalidAmount ? 'red.600' : 'border.gray'}
        value={amount}
        onChange={(e) => setAmount(e.target.value)}
      />
      <Button
        pos={'absolute'}
        h={'full'}
        w={'20%'}
        right={0}
        bg={'bg.gray'}
        color={'text.gray'}
        border={'1px solid'}
        borderColor={isInvalidAmount ? 'red.600' : 'border.gray'}
        _hover={{ bg: 'bg.gray', color: 'black' }}
        zIndex={1}
        onClick={() => setAmount(maxAmount)}
      >
        Max
      </Button>
    </Flex>
  )
}
