import { useDepositStore, usePoolsStore } from '@/stores'
import { NumberUtil } from '@/utils'
import { Divider, Flex, Stack, Text } from '@chakra-ui/react'

export const DepositSummary = () => {
  const { pool, balance } = useDepositStore()

  return (
    <Stack px={2}>
      <Flex justifyContent={'space-between'}>
        <Text color={'text.gray'} fontSize={14}>
          Fixed USDC APY
        </Text>
        <Text fontWeight={'bold'}>{pool?.apy?.toFixed(2)}%</Text>
      </Flex>
      <Divider borderColor={'border.gray'} />
      <Flex justifyContent={'space-between'}>
        <Text color={'text.gray'} fontSize={14}>
          Available
        </Text>
        <Text fontWeight={'bold'}>{NumberUtil.formatThousands(pool?.available)} USDC</Text>
      </Flex>
      <Divider borderColor={'border.gray'} />
      <Flex justifyContent={'space-between'}>
        <Text color={'text.gray'} fontSize={14}>
          Wallet balance
        </Text>
        <Text fontWeight={'bold'}>{NumberUtil.toFixed(balance?.formatted, 2)} USDC</Text>
      </Flex>
    </Stack>
  )
}
