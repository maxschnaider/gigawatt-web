'use client'

import { Footer, Header } from '@/components'
import { Flex, FlexProps } from '@chakra-ui/react'
import { Suspense } from 'react'
import Loading from '@/app/loading'

type IMainLayout = FlexProps

export const MainLayout = ({ children, ...props }: IMainLayout) => (
  <Flex
    id='main'
    flexDir={'column'}
    w={'full'}
    h={'full'}
    maxW={'1440px'}
    margin={'0 auto'}
    overflowX={'hidden'}
    gap={{ sm: '60px', md: '100px' }}
    py={{ sm: 4, md: 8 }}
    {...props}
  >
    <Header />
    <Suspense fallback={<Loading />}>{children}</Suspense>
    <Footer />
  </Flex>
)
