import { TranchePoolABI } from '@/contracts'
import { defineConfig, loadEnv } from '@wagmi/cli'
import { actions, blockExplorer, react } from '@wagmi/cli/plugins'
import { erc20ABI } from 'wagmi'

// @ts-ignore
export default defineConfig(() => {
  const env = loadEnv({
    mode: process.env.NODE_ENV,
    envDir: process.cwd(),
  })
  return {
    out: 'src/contracts/generated.ts',
    contracts: [
      {
        name: 'erc20',
        abi: erc20ABI,
      },
      {
        name: 'TranchePool',
        abi: TranchePoolABI,
      },
    ],
    plugins: [
      // blockExplorer({
      //   apiKey: env.EXPLORER_API_KEY!,
      //   baseUrl: DEFAULT_CHAIN.blockExplorers.default.api,
      //   contracts: [GIGAWATT_CONTRACTS.TranchePool],
      // }),
      react(),
      // actions()
    ],
  }
})
