import { Container } from '@/components'
import { Divider, Flex, FlexProps, Heading, Stack, Text, TextProps } from '@chakra-ui/react'

export const MainPageStatsTable = () => {
  const StatsTableSubheader = ({ children, ...props }: TextProps) => (
    <Flex flexBasis={'100%'} justifyContent={'center'} textAlign={'center'}>
      <Text textTransform={'uppercase'} fontSize={{ sm: 11, md: 14 }} {...props}>
        {children}
      </Text>
    </Flex>
  )

  const StatsTableItem = ({ children, ...props }: FlexProps) => (
    <Flex
      flexBasis={'100%'}
      alignItems={'center'}
      justifyContent={'center'}
      textAlign={'center'}
      p={4}
      {...props}
    >
      <Heading textTransform={'uppercase'} fontSize={{ sm: 16, md: 24 }}>
        {children}
      </Heading>
    </Flex>
  )

  const StatsTableDivider = () => <Divider orientation='vertical' borderColor={'border.gray'} />

  const StatsTable = () => (
    <Flex
      flexBasis={'100%'}
      flexDir={'column'}
      gap={4}
      alignItems={'center'}
      display={{ sm: 'none', md: 'flex' }}
    >
      <Stack w={'full'} flexDir={'row'} justifyContent={'space-around'} px={2}>
        <StatsTableSubheader>Capacity, USDC</StatsTableSubheader>
        <StatsTableSubheader>gigapool x apy</StatsTableSubheader>
        <StatsTableSubheader>loans repaid</StatsTableSubheader>
        <StatsTableSubheader>annual co₂ savings</StatsTableSubheader>
      </Stack>
      <Container w={'full'} h={{ sm: '100px', md: '220px' }} flexDir={'column'} p={0}>
        <Stack flexDir={'row'} w={'full'} h={'full'} justifyContent={'space-around'} spacing={0}>
          <StatsTableItem>$71 M</StatsTableItem>
          <StatsTableDivider />
          <StatsTableItem>11%</StatsTableItem>
          <StatsTableDivider />
          <StatsTableItem>$8.11 M</StatsTableItem>
          <StatsTableDivider />
          <StatsTableItem>284 MTONS</StatsTableItem>
        </Stack>
      </Container>
    </Flex>
  )

  const StatsTableMobileSubheader = ({ children, ...props }: TextProps) => (
    <Text fontSize={12} color={'text.gray'} {...props}>
      {children}
    </Text>
  )

  const StatsTableMobileItem = ({
    k,
    v,
    ...props
  }: { k: any; v: any } & Omit<FlexProps, 'children'>) => (
    <Container w={'full'} flexDir={'column'} {...props}>
      <StatsTableMobileSubheader>{k}</StatsTableMobileSubheader>
      <Heading fontSize={18}>{v}</Heading>
    </Container>
  )

  const StatsTableMobile = () => (
    <Stack spacing={0} display={{ sm: 'flex', md: 'none' }}>
      <Flex>
        <StatsTableMobileItem
          k={`Capacity, USDC`}
          v={`$71 M`}
          p={{ sm: '16px 16px 20px', md: '20px 20px 24px' }}
          borderRadius={'12px 0 0 0'}
        />
        <StatsTableMobileItem
          k={`Gigapool X APY`}
          v={`11%`}
          p={{ sm: '16px 16px 20px', md: '20px 20px 24px' }}
          borderRadius={'0 12px 0 0'}
          borderLeft={'none'}
        />
      </Flex>
      <Flex>
        <StatsTableMobileItem
          k={`Loans repaid`}
          v={`$8.11 M`}
          p={{ sm: '16px 16px 20px', md: '20px 20px 24px' }}
          borderRadius={'0 0 0 12px'}
          borderTop={'none'}
        />
        <StatsTableMobileItem
          k={`Annual CO₂ savings`}
          v={`284 MTONS`}
          p={{ sm: '16px 16px 20px', md: '20px 20px 24px' }}
          borderRadius={'0 0 12px 0'}
          borderTop={'none'}
          borderLeft={'none'}
        />
      </Flex>
    </Stack>
  )

  return (
    <>
      <StatsTable />
      <StatsTableMobile />
    </>
  )
}
