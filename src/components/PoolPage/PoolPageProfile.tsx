import { Container, DepositButton } from '@/components'
import { usePoolsStore } from '@/stores'
import { Divider, Flex, Heading, Stack, Text } from '@chakra-ui/react'

export const PoolPageProfile = () => {
  const { poolSelected: pool } = usePoolsStore()

  return (
    <Container
      flexBasis={'69%'}
      w={'full'}
      flexDir={'column'}
      gap={{ sm: '25px', md: '40px' }}
      p={{ sm: '20px 16px', md: '32px' }}
      h={'fit-content'}
    >
      <Heading fontSize={{ sm: 24, md: 28 }} pr={{ sm: 0, md: '20%' }}>
        Lend East #1: Emerging Asia Fintech Pool
      </Heading>
      <Text>{`Proceeds will be used for additional funding via existing Lend East Credit Facilities to our portfolio companies and funding of new alternate lending platforms in Emerging Asia. All Lend East Credit Facilities are senior secured loans to high growth, tech forward Alternate Lenders who are backed by marquee equity investors like Ant Financial, Sequoia Capital, DST Global, Sinar Mas, Quona Capital & Arbor Ventures. Every facility is structured to provide a healthy risk adjusted return to investors while ensuring capital preservation. Typical guardrails include excess portfolio cover (1.2x - 1.5x of the investment), established seniority over equity & other debt capital on the balance sheet and corporate guarantees from Holding Companies.`}</Text>

      <Stack spacing={5} fontSize={{ sm: 16, md: 17 }}>
        <Flex justifyContent={'space-between'}>
          <Stack>
            <Text fontSize={12} color={'text.gray'}>
              Fixed USDC APY
            </Text>
            <Heading fontSize={{ sm: 24, md: 28 }}>{pool?.apy?.toFixed(2)}%</Heading>
          </Stack>
          {/* <Stack alignItems={'end'}>
          <Text fontSize={12} color={'text.gray'}>
            Variable GFI APY
          </Text>
          <Heading fontSize={28}>1.67%</Heading>
        </Stack> */}
        </Flex>
        <Divider borderColor={'border.gray'} />
        <Flex justifyContent={'space-between'}>
          <Text>Loan term</Text>
          <Text fontWeight={'bold'}>25 months</Text>
        </Flex>
        <Divider borderColor={'border.gray'} />
        <Flex justifyContent={'space-between'}>
          <Text>Liquidity</Text>
          <Text fontWeight={'bold'}>End of loan term</Text>
        </Flex>
      </Stack>

      <DepositButton />
    </Container>
  )
}
