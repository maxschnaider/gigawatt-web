import { Flex, Heading, Image, Text, VStack } from '@chakra-ui/react'

export const Footer = () => (
  <Flex
    flexDir={'column'}
    justifyContent={'space-between'}
    gap={{ sm: '69px', md: '100px' }}
    pt={{ sm: 0, md: '80px' }}
    pb={{ sm: '15px', md: '20px' }}
    px={{ sm: '15px', md: '24px', xxl: 0 }}
  >
    <Flex
      justifyContent={'right'}
      gap={{ sm: '50px', md: '100px' }}
      flexDir={{ sm: 'column', md: 'row' }}
    >
      <VStack alignItems={{ sm: 'center', md: 'start' }}>
        <Heading size={'sm'} mb={4}>
          Contacts
        </Heading>
        <Text>gm@gigawa.tt</Text>
        <Text>+62 666 888 32</Text>
      </VStack>
      <VStack alignItems={{ sm: 'center', md: 'start' }}>
        <Heading size={'sm'} mb={4}>
          Socials
        </Heading>
        <Text>Twitter</Text>
        <Text>Discord</Text>
        <Text>Github</Text>
      </VStack>
      <VStack alignItems={{ sm: 'center', md: 'start' }}>
        <Heading size={'sm'} mb={4}>
          Menu
        </Heading>
        <Text>About</Text>
        <Text>Whitepaper</Text>
        <Text>Careers</Text>
        <Text>Terms</Text>
      </VStack>
    </Flex>
    <Flex
      justifyContent={'space-between'}
      alignItems={'center'}
      color={'text.gray'}
      flexDir={{ sm: 'column-reverse', md: 'row' }}
      gap={'10px'}
    >
      <Image src='/assets/images/gigawatt-logo-small.svg' h={'30px'} />
      <Text>GGWT, 2023</Text>
      <Text>All rights reserved</Text>
    </Flex>
  </Flex>
)
