import { CHAINS_SUPPORTED_BY_NAME } from '@/constants'
import { Token } from '@/types'

export const MOCK_ERC20: Token = {
  address: {
    [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0x579438a8debdf77ef79bde9e5ea3ec68cfab8657',
  },
  symbol: 'MCK',
  decimals: 18,
}

export const USDC: Token = {
  address: {
    [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0x',
  },
  symbol: 'USDC',
  decimals: 6,
}
