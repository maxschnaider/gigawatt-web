'use client'

import * as React from 'react'
import { ChakraProvider, RainbowKitProvider, StoreProviders, WagmiProvider } from '@/providers'

export const Providers = ({ children }: React.PropsWithChildren) => {
  const [mounted, setMounted] = React.useState(false)
  React.useEffect(() => setMounted(true), [])

  return (
    <ChakraProvider>
      <WagmiProvider>
        <RainbowKitProvider>
          <StoreProviders>{mounted && children}</StoreProviders>
        </RainbowKitProvider>
      </WagmiProvider>
    </ChakraProvider>
  )
}
