import { MOCK_ERC20 } from '@/constants'
import { useErc20Allowance } from '@/contracts/generated'
import { useChain } from '@/hooks'
import { Address, Token } from '@/types'
import { Logger } from '@/utils'
import { formatUnits, zeroAddress } from 'viem'
import { useAccount, useQuery } from 'wagmi'

interface IUseAllowance {
  token?: Token
  spender?: Address
  refetchInterval?: false | number
}

export const useAllowance = ({
  token = MOCK_ERC20,
  spender = zeroAddress,
  refetchInterval = 3000,
}: IUseAllowance) => {
  const logger = new Logger(useAllowance.name)

  const { address } = useAccount()
  const owner = address ?? zeroAddress
  const { chain } = useChain()
  const tokenAddress = token.address[chain.id]

  const query = useErc20Allowance({
    address: tokenAddress,
    args: [owner, spender],
    onSuccess: (allowance) => {
      logger.success(token, owner, spender, formatUnits(allowance, token.decimals))
    },
  })

  useQuery(['allowance-interval-query', tokenAddress, owner, spender], {
    queryFn: query.refetch,
    refetchInterval,
  })

  return query
}
