import { Analytics } from '@vercel/analytics/react'

export const metadata = {
  title: 'Gigawatt',
  icons: {
    icon: 'data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 100 100%22><text y=%22.9em%22 font-size=%2290%22>⚡️</text></svg>',
  },
}

const RootLayout = ({ children }: React.PropsWithChildren) => {
  return (
    <html lang='en'>
      <body style={{ overflowX: 'hidden', userSelect: 'none' }}>
        {children}
        <Analytics />
      </body>
    </html>
  )
}

export default RootLayout
