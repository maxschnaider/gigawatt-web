import { DepositStoreProvider, PoolsStoreProvider } from '@/stores'

export const StoreProviders = ({ children }: React.PropsWithChildren) => (
  <PoolsStoreProvider>
    <DepositStoreProvider>{children}</DepositStoreProvider>
  </PoolsStoreProvider>
)
