import { MOCK_ERC20 } from '@/constants'
import { useErc20Approve } from '@/contracts/generated'
import { useChain } from '@/hooks'
import { Address, Token } from '@/types'
import { Logger } from '@/utils'
import { TransactionReceipt, zeroAddress } from 'viem'
import { useWaitForTransaction } from 'wagmi'
import { WriteContractResult } from 'wagmi/dist/actions'

interface IUseApprove {
  token?: Token
  spender?: Address
  amount: bigint
  onSign?: (tx: WriteContractResult) => void
  onConfirm?: (receipt: TransactionReceipt) => void
}

export const useApprove = ({
  token = MOCK_ERC20,
  spender = zeroAddress,
  amount,
  onSign,
  onConfirm,
}: IUseApprove) => {
  const logger = new Logger(useApprove.name)

  const { chain } = useChain()
  const tokenAddress = token.address[chain.id]

  const {
    data: tx,
    isLoading: isLoadingSign,
    write: mutate,
  } = useErc20Approve({
    address: tokenAddress,
    args: [spender, amount],
    onSuccess: (tx) => {
      logger.success('TX_SIGNED', tokenAddress, spender, amount, tx.hash)
      onSign?.(tx)
    },
  })

  const {
    data: receipt,
    isLoading: isLoadingConfirm,
    isSuccess,
  } = useWaitForTransaction({
    hash: tx?.hash,
    onSuccess: (receipt) => {
      logger.success('TX_CONFIRMED', receipt)
      onConfirm?.(receipt)
    },
  })

  return {
    mutate,
    data: { tx, receipt },
    isLoadingSign,
    isLoadingConfirm,
    isLoading: isLoadingSign || isLoadingConfirm,
    isSuccess,
  } as const
}
