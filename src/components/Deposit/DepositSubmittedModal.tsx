import { Button, Modal } from '@/components'
import { useChain } from '@/hooks'
import { useDepositStore } from '@/stores'
import { Link, Stack } from '@chakra-ui/react'

export const DepositSubmittedModal = () => {
  const { isOpenDepositSubmittedModal, closeDepositSubmittedModal, depositTx } = useDepositStore()
  const { chain } = useChain()

  const ExplorerButton = () => (
    <Link isExternal href={`${chain.blockExplorers?.default.url}/tx/${depositTx?.hash}`}>
      <Button w={'full'}>View on explorer</Button>
    </Link>
  )
  const DoneButton = () => (
    <Button
      bg={'blackAlpha.500'}
      _hover={{ bg: 'blackAlpha.400' }}
      onClick={closeDepositSubmittedModal}
    >
      Done
    </Button>
  )

  return (
    <Modal
      title={'Submitted'}
      isOpen={isOpenDepositSubmittedModal}
      onClose={closeDepositSubmittedModal}
      size={'sm'}
    >
      <Stack>
        <ExplorerButton />
        <DoneButton />
      </Stack>
    </Modal>
  )
}
