import * as chains from 'wagmi/chains'

export const CHAINS_SUPPORTED_BY_NAME = {
  Mumbai: {
    ...chains.polygonMumbai,
    blockExplorers: {
      default: {
        name: 'PolygonScan',
        url: 'https://mumbai.polygonscan.com',
        api: 'https://api-testnet.polygonscan.com/api',
      },
    },
    unsupported: false,
  },
  // Polygon: {
  //   ...chains.polygon,
  //   blockExplorers: {
  //     default: {
  //       name: 'PolygonScan',
  //       url: 'https://polygonscan.com',
  //       api: 'https://api.polygonscan.com/api',
  //     },
  //   },
  //   unsupported: false,
  // },
}

export const CHAINS_SUPPORTED = Object.values(CHAINS_SUPPORTED_BY_NAME)

export const CHAIN_IDS_SUPPORTED: number[] = CHAINS_SUPPORTED.map((chain) => chain.id)

export const DEFAULT_CHAIN =
  CHAINS_SUPPORTED.find((chain) => chain.id === Number(process.env.NEXT_PUBLIC_DEFAULT_CHAIN_ID)) ??
  CHAINS_SUPPORTED[0]
// process.env.NODE_ENV === 'development' ? CHAINS_SUPPORTED_BY_NAME.Mumbai : CHAINS_SUPPORTED_BY_NAME.Celo
