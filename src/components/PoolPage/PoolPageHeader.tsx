import { Button } from '@/components'
import { Button as ChakraButton, Flex } from '@chakra-ui/react'
import { useRouter } from 'next/navigation'
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa'

export const PoolPageHeader = () => {
  const router = useRouter()

  return (
    <Flex justifyContent={'space-between'} gap={2} alignItems={'end'}>
      <ChakraButton
        variant={'link'}
        colorScheme='blackAlpha'
        gap={2}
        alignItems={'center'}
        fontSize={14}
        textTransform={'uppercase'}
        py={1}
        onClick={() => router.push('/')}
      >
        <FaArrowLeft />
        Back to Pools
      </ChakraButton>
      <Button
        px={{ sm: '24px', md: '40px' }}
        h={{ sm: '45px', md: '55px' }}
        // onClick={() => router.push('/')}
      >
        Dataroom
        <FaArrowRight />
      </Button>
    </Flex>
  )
}
