'use client'

import {
  DepositModals,
  PoolPageHeader,
  PoolPageOverviewAndHighlights,
  PoolPageProfile,
} from '@/components'
import { usePoolsStore } from '@/stores'
import { Flex, Stack } from '@chakra-ui/react'
import { useEffect } from 'react'

interface IPoolPage {
  params: { poolId: string }
}

const PoolPage = ({ params }: IPoolPage) => {
  const { selectPool } = usePoolsStore()
  useEffect(() => {
    selectPool(params.poolId)
  }, [])

  return (
    <Stack px={{ sm: '15px', md: '24px', xxl: 0 }} spacing={6}>
      <PoolPageHeader />
      <Flex
        flexDir={{ sm: 'column', md: 'row' }}
        gap={{ sm: '50px', xxl: '60px' }}
        justifyContent={'space-between'}
      >
        <PoolPageProfile />
        <PoolPageOverviewAndHighlights />
      </Flex>

      <DepositModals />
    </Stack>
  )
}

export default PoolPage
