import { CHAINS_SUPPORTED_BY_NAME } from '@/constants'
import { Address } from '@/types'

type ContractInfo = {
  name: string
  address: { [chainId: number]: Address }
}

type GigawattContracts = 'GigawattFactory' | 'TranchePool' | 'RmwNFT'
export const GIGAWATT_CONTRACTS: {
  [contractName in GigawattContracts]: ContractInfo
} = {
  GigawattFactory: {
    name: 'GigawattFactory',
    address: {
      [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0xe3C9edF8dd3B23cA960Cc4fA8a100f1f0629248e',
    },
  },
  TranchePool: {
    name: 'TranchePool',
    address: { [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0xa2978b6b29764c1d9821bad52fea23f100ade10f' },
  },
  RmwNFT: {
    name: 'RmwNFT',
    address: { [CHAINS_SUPPORTED_BY_NAME.Mumbai.id]: '0x66a561d6E98D10911A967254FcDFDDFfa1b57AA7' },
  },
}
