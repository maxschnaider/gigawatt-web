'use client'

import { Flex, FlexProps, Image } from '@chakra-ui/react'
// import { FaBars } from 'react-icons/fa'
import { useRouter } from 'next/navigation'
import { ConnectButton } from '@/components'

export const Header = () => {
  const router = useRouter()

  const Logo = ({ ...props }: FlexProps) => (
    <Flex
      pos={'absolute'}
      top={'45%'}
      left={'50%'}
      transform={'translate(-50%, -50%)'}
      _hover={{ opacity: 0.69 }}
      cursor={'pointer'}
      onClick={() => router.push('/')}
      {...props}
    >
      <Image src='/assets/images/gigawatt-logo.svg' h={'30px'} />
    </Flex>
  )

  const LogoMobile = ({ ...props }: FlexProps) => (
    <Flex cursor={'pointer'} onClick={() => router.push('/')} {...props}>
      <Image src='/assets/images/gigawatt-logo-small.svg' h={'30px'} />
    </Flex>
  )

  return (
    <Flex
      justifyContent={'space-between'}
      alignItems={{ sm: 'normal', md: 'center' }}
      h={'40px'}
      px={{ sm: '15px', md: '24px', xxl: 0 }}
      pos={'relative'}
      gap={4}
    >
      <Flex display={{ sm: 'none', md: 'block' }}>{/* <FaBars fontSize={'20px'} /> */}</Flex>
      <>
        <Logo display={{ sm: 'none', md: 'flex' }} />
        <LogoMobile display={{ sm: 'flex', md: 'none' }} />
      </>
      <ConnectButton h={'40px'} fontSize={15} />
    </Flex>
  )
}
