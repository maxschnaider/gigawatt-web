import '@rainbow-me/rainbowkit/styles.css'
import {
  RainbowKitProvider as RainbowKitDefaultProvider,
  connectorsForWallets,
} from '@rainbow-me/rainbowkit'
import {
  metaMaskWallet,
  injectedWallet,
  walletConnectWallet,
  coinbaseWallet,
  rainbowWallet,
  // braveWallet,
  trustWallet,
  // safeWallet,
} from '@rainbow-me/rainbowkit/wallets'
import { CHAINS_SUPPORTED as chains } from '@/constants'
import { rainbowKitTheme } from '@/styles'

const projectId = process.env.NEXT_PUBLIC_WALLET_CONNECT_PROJECT_ID ?? ''

export const connectors = connectorsForWallets([
  {
    groupName: 'Popular',
    wallets: [
      metaMaskWallet({ chains, projectId, shimDisconnect: true }),
      injectedWallet({ chains, shimDisconnect: true }),
      walletConnectWallet({ chains, projectId }),
    ],
  },
  {
    groupName: 'More',
    wallets: [
      // safeWallet({ chains }),
      rainbowWallet({ chains, projectId, shimDisconnect: true }),
      coinbaseWallet({ appName: 'Gigawatt', chains }),
      // braveWallet({ chains, shimDisconnect: true }),
      trustWallet({ chains, projectId, shimDisconnect: true }),
    ],
  },
])

export const RainbowKitProvider = ({ children }: React.PropsWithChildren) => (
  <RainbowKitDefaultProvider
    appInfo={{
      appName: projectId,
    }}
    chains={chains}
    modalSize={'compact'}
    theme={rainbowKitTheme}
  >
    {children}
  </RainbowKitDefaultProvider>
)
