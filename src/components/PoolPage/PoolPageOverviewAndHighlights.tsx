import { Container } from '@/components'
import { usePoolsStore } from '@/stores'
import { Flex, FlexProps, Heading, Stack, Text, TextProps } from '@chakra-ui/react'

export const PoolPageOverviewAndHighlights = () => {
  const { poolSelected: pool } = usePoolsStore()

  const OverviewSubheader = ({ children, ...props }: TextProps) => (
    <Text fontSize={12} color={'text.gray'} {...props}>
      {children}
    </Text>
  )

  const OverviewItem = ({ k, v, ...props }: { k: any; v: any } & Omit<FlexProps, 'children'>) => (
    <Container w={'full'} flexDir={'column'} {...props}>
      <OverviewSubheader>{k}</OverviewSubheader>
      <Heading fontSize={{ sm: 18, md: 24 }}>{v}</Heading>
    </Container>
  )

  const Overview = () => (
    <Stack spacing={{ sm: 6, md: 8 }}>
      <Heading fontSize={{ sm: 18, md: 24 }} textAlign={{ sm: 'center', md: 'left' }}>
        Overview
      </Heading>
      <Stack spacing={0}>
        <Flex>
          <OverviewItem
            k={'Fixed USDC APY'}
            v={`${pool?.apy?.toFixed(2)}%`}
            p={{ sm: '16px 16px 20px', md: '20px 20px 24px' }}
            borderRadius={'12px 0 0 0'}
          />
          <OverviewItem
            k={'Available'}
            v={`215,000 USDC`}
            p={{ sm: '16px 16px 20px', md: '20px 20px 24px' }}
            borderRadius={'0 12px 0 0'}
            borderLeft={'none'}
          />
        </Flex>
        <Flex>
          <OverviewItem
            k={'Annual CO2 savings'}
            v={`12B MTons`}
            p={{ sm: '16px 16px 20px', md: '20px 20px 24px' }}
            borderRadius={'0 0 0 12px'}
            borderTop={'none'}
          />
          <OverviewItem
            k={'Capacity, Watt'}
            v={`12,4 GW`}
            p={{ sm: '16px 16px 20px', md: '20px 20px 24px' }}
            borderRadius={'0 0 12px 0'}
            borderTop={'none'}
            borderLeft={'none'}
          />
        </Flex>
      </Stack>
    </Stack>
  )

  const HighlightsItemXL = ({
    k,
    v,
    ...props
  }: { k: any; v: any } & Omit<FlexProps, 'children'>) => (
    <Container
      w={'full'}
      p={{ sm: '20px 20px 24px', md: '32px' }}
      gap={4}
      flexDir={{ sm: 'column', md: 'row' }}
      {...props}
    >
      <Heading flexBasis={'60%'} fontSize={{ sm: 19, md: 24 }}>
        {k}
      </Heading>
      <Text flexBasis={'100%'}>{v}</Text>
    </Container>
  )

  const HighlightsItem = ({ k, v, ...props }: { k: any; v: any } & Omit<FlexProps, 'children'>) => (
    <Container
      w={'full'}
      flexDir={'column'}
      gap={4}
      p={{ sm: '20px 20px 24px', md: '24px 32px 28px' }}
      {...props}
    >
      <Heading fontSize={{ sm: 17, md: 18 }}>{k}</Heading>
      <Text>{v}</Text>
    </Container>
  )

  const Highlights = () => (
    <Stack spacing={{ sm: 6, md: 8 }}>
      <Heading fontSize={{ sm: 18, md: 24 }} textAlign={{ sm: 'center', md: 'left' }}>
        Highlights
      </Heading>
      <Stack spacing={4}>
        <HighlightsItemXL
          k={`Asian Fintech Exposure`}
          v={`Proceeds from this pool will be used to fund experienced fintech lenders in Southeast
            Asia - one of the fastest-growing regions for fintech adoption in the world`}
        />
        <Flex justifyContent={'space-between'} flexDir={{ sm: 'column', md: 'row' }} gap={4}>
          <HighlightsItem k={'Secured'} v={`Collateralized with real-world (off-chain) assets`} />
          <HighlightsItem
            k={'Real-world recourse'}
            v={`Real-world, legally enforceable loan agreement`}
          />
          <HighlightsItem
            k={'Ongoing monitoring'}
            v={`Monthly reporting and direct-to-borrower communications`}
          />
        </Flex>
      </Stack>
    </Stack>
  )

  return (
    <Stack flexBasis={'100%'} spacing={'50px'}>
      <Overview />
      <Highlights />
    </Stack>
  )
}
