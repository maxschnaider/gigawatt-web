import { createContext, useContext, useMemo, useState } from 'react'
import { useDisclosure } from '@chakra-ui/react'
import { useAccount } from 'wagmi'
import { FetchBalanceResult, WriteContractResult } from 'wagmi/dist/actions'
import { formatUnits, parseUnits } from 'viem'
import { useAllowance, useApprove, useBalance, useChain, useDeposit } from '@/hooks'
import { MOCK_ERC20 } from '@/constants'
import { usePoolsStore } from '@/stores'
import { DepositUIState, Pool } from '@/types'

interface DepositStoreState {
  pool?: Pool
  balance?: FetchBalanceResult

  amount?: string
  setAmount: (amount?: string) => void
  maxAmount: string

  state: DepositUIState

  approve: () => void

  deposit: () => void
  depositTx?: WriteContractResult

  isOpenDepositModal: boolean
  openDepositModal: () => void
  closeDepositModal: () => void
  isOpenDepositSubmittedModal: boolean
  openDepositSubmittedModal: () => void
  closeDepositSubmittedModal: () => void
}

const DepositStore = createContext<DepositStoreState>({} as DepositStoreState)

export const DepositStoreProvider = ({ children }: React.PropsWithChildren) => {
  /**
   * PARENT STORE
   */
  const { poolSelected: pool } = usePoolsStore()

  /**
   * WALLET
   */
  const { isConnected } = useAccount()
  const { chain, activeChain } = useChain()

  /**
   * BALANCE
   */
  const { data: balance } = useBalance()

  /**
   * AMOUNT
   */
  const [amount, setAmount] = useState<string | undefined>()
  const amountBI = useMemo(
    () => parseUnits(amount ?? '0', MOCK_ERC20.decimals),
    [amount, MOCK_ERC20]
  )

  const maxAmountBI = useMemo(() => {
    if (!pool || !balance) return 0n
    const availableBI = parseUnits(pool.available.toString(), MOCK_ERC20.decimals)
    const balanceBI = balance.value
    return balanceBI < availableBI ? balanceBI : availableBI
  }, [pool, balance, MOCK_ERC20])
  const maxAmount = useMemo(
    () => formatUnits(maxAmountBI, MOCK_ERC20.decimals),
    [maxAmountBI, MOCK_ERC20]
  )

  const isInvalidAmount = amountBI <= 0 || amountBI > maxAmountBI

  /**
   * ALLOWANCE & APPROVE
   */
  // const spender = GIGAWATT_CONTRACTS.TranchePool.address[chain.id] ?? zeroAddress
  const spender = pool?.address[chain.id]

  const { data: allowance } = useAllowance({
    spender,
  })

  const isExceedsAllowance = amountBI > (allowance ?? 0n)

  const { mutate: approve, isLoading: isLoadingApprove } = useApprove({
    spender,
    amount: amountBI,
  })

  /**
   * DEPOSIT
   */
  const {
    mutate: deposit,
    isLoadingSign: isLoadingDepositSign,
    data: { tx: depositTx },
  } = useDeposit({
    pool,
    amount: amountBI,
    onSign: () => {
      closeDepositModal()
      openDepositSubmittedModal()
    },
  })

  /**
   * UI STATE
   */
  const uiState: DepositUIState = useMemo(() => {
    if (!isConnected) return DepositUIState.WalletNotConnected
    if (activeChain?.unsupported) return DepositUIState.WrongNetwork
    if (isInvalidAmount) return DepositUIState.InvalidAmount
    if (isLoadingApprove || isLoadingDepositSign) return DepositUIState.Loading
    if (isExceedsAllowance) return DepositUIState.ExceedsAllowance
    if (depositTx) return DepositUIState.DepositSubmitted
    return DepositUIState.ReadyToDeposit
  }, [
    isConnected,
    activeChain,
    isInvalidAmount,
    isExceedsAllowance,
    isLoadingApprove,
    isLoadingDepositSign,
    depositTx,
  ])

  /**
   * MODALS UI
   */
  const {
    isOpen: isOpenDepositModal,
    onOpen: openDepositModal,
    onClose: closeDepositModal,
  } = useDisclosure()

  const {
    isOpen: isOpenDepositSubmittedModal,
    onOpen: openDepositSubmittedModal,
    onClose: closeDepositSubmittedModal,
  } = useDisclosure()

  const state: DepositStoreState = {
    pool,
    balance,
    amount,
    setAmount,
    maxAmount,
    state: uiState,
    approve,
    deposit,
    depositTx,
    isOpenDepositModal,
    openDepositModal,
    closeDepositModal,
    isOpenDepositSubmittedModal,
    openDepositSubmittedModal,
    closeDepositSubmittedModal,
  }

  return <DepositStore.Provider value={state}>{children}</DepositStore.Provider>
}

export const useDepositStore = () => useContext(DepositStore)
