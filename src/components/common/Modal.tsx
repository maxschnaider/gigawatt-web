import {
  Button,
  Heading,
  Modal as ChakraModal,
  ModalBody,
  ModalContent,
  ModalContentProps,
  ModalHeader,
  ModalOverlay,
  ModalProps,
  Flex,
} from '@chakra-ui/react'

export type IModal = ModalProps &
  ModalContentProps & {
    title?: string
    showCloseButton?: boolean
  }

export const Modal = ({
  title,
  showCloseButton = true,
  onClose,
  isOpen,
  children,
  size,
  ...props
}: IModal) => (
  <ChakraModal onClose={onClose} isOpen={isOpen} size={size} isCentered>
    <ModalOverlay />
    <ModalContent
      borderRadius='12px'
      bg={'bg.gray'}
      border={'1px solid'}
      borderColor={'border.gray'}
      p={'20px 20px 24px'}
      mx={{ sm: '8px', md: '20px' }}
      gap={8}
      {...props}
    >
      <ModalHeader display='flex' justifyContent='space-between' p={0}>
        <Flex w={'26px'} />
        <Heading fontSize={{ sm: '20px', md: '24px' }}>{title}</Heading>
        {showCloseButton && (
          <Button
            w={'26px'}
            h={'26px'}
            color={'text.gray'}
            bg={'blackAlpha.200'}
            colorScheme={'none'}
            borderRadius={'full'}
            size={'xs'}
            fontSize={13}
            onClick={onClose}
            transition={'0.2s'}
            _hover={{ transform: 'scale(1.1)' }}
            _active={{ transform: 'none' }}
          >
            ⛌
          </Button>
        )}
      </ModalHeader>
      <ModalBody p={0}>{children}</ModalBody>
    </ModalContent>
  </ChakraModal>
)
