'use client'

import { Input as ChakraInput, InputProps } from '@chakra-ui/react'

export const Input = ({ ...props }: InputProps) => (
  <ChakraInput
    fontStyle={'italic'}
    outline={'none'}
    border={'1px solid'}
    borderColor={props.borderColor ?? 'border.gray'}
    borderRadius={'full'}
    h={'50px'}
    _placeholder={{ color: 'black', opacity: 0.42 }}
    _hover={{ border: '1px solid', borderColor: props.borderColor ?? 'border.gray' }}
    _focus={{
      boxShadow: 'none',
      border: '1px solid',
      borderColor: props.borderColor ?? 'border.gray',
    }}
    {...props}
  />
)
