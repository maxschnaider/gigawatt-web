import { useMemo, useRef } from 'react'
import { Button, Container } from '@/components'
import { Pool } from '@/types'
import {
  Divider,
  Flex,
  HStack,
  Heading,
  VStack,
  Text,
  FlexProps,
  useMediaQuery,
} from '@chakra-ui/react'
import { FaArrowLeft, FaArrowRight } from 'react-icons/fa'
import { Swiper, SwiperRef, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import { useRouter } from 'next/navigation'
import { usePoolsStore } from '@/stores'

const POOL_CARD_HEIGHT = 340

export const MainPagePools = () => {
  const { pools, selectPool } = usePoolsStore()
  const router = useRouter()

  const [isMobile, isXXL] = useMediaQuery(['(max-width: 480px)', '(min-width: 1500px)'])
  const offsetWidth = useMemo(() => {
    if (isMobile) return 15
    if (!isXXL) return 24
    return (document.documentElement.clientWidth - 1440) / 2
  }, [isMobile, isXXL, document.documentElement.clientWidth])

  const swiper = useRef<SwiperRef>(null)

  const Header = () => (
    <Flex
      justifyContent={'space-between'}
      w={'full'}
      px={{ sm: '15px', md: '24px', xxl: 0 }}
      alignItems={'center'}
    >
      <Flex w={'60px'} display={{ sm: 'none', md: 'flex' }} />
      <Heading textTransform={'uppercase'} fontSize={{ sm: 18, md: 28 }}>
        Explore our pools
      </Heading>
      <HStack>
        <Flex
          h={'28px'}
          w={'28px'}
          justifyContent={'center'}
          alignItems={'center'}
          borderRadius={'full'}
          cursor={'pointer'}
          onClick={() => swiper.current?.swiper?.slidePrev()}
        >
          <FaArrowLeft fontSize={12} />
        </Flex>
        <Flex
          h={'28px'}
          w={'28px'}
          justifyContent={'center'}
          alignItems={'center'}
          borderRadius={'full'}
          border={'1px solid black'}
          cursor={'pointer'}
          onClick={() => swiper.current?.swiper?.slideNext()}
        >
          <FaArrowRight fontSize={12} />
        </Flex>
      </HStack>
    </Flex>
  )

  const Pool = ({ pool, ...props }: { pool: Pool } & FlexProps) => {
    const PoolKeyValue = ({ k, v }: { k?: any; v?: any }) => (
      <HStack w={'full'} justifyContent={'space-between'}>
        <Text color={'#97989B'} fontSize={14}>
          {k}:
        </Text>
        <Text fontSize={14} textAlign={'right'}>
          {v}
        </Text>
      </HStack>
    )
    return (
      <Container
        p={0}
        w={'280px !important'}
        h={`${POOL_CARD_HEIGHT}px !important`}
        cursor={'pointer'}
        overflow={'auto'}
        onClick={() => {
          selectPool(pool.id)
          router.push(`/pool/${pool.id}`)
        }}
        {...props}
      >
        <VStack spacing={0} w={'full'}>
          <Flex w={'full'} justifyContent={'space-between'} p={'20px 20px 10px'} gap={2}>
            <Heading fontSize={18}>{pool.name}</Heading>
            <Text color={'text.gray'} fontSize={12} flexBasis={'50%'} textAlign={'right'}>
              Diversified Global Exposure
            </Text>
          </Flex>
          <Divider borderColor={'border.gray'} />
          <VStack w={'full'} p={'20px'} alignItems={'start'} spacing={4}>
            <VStack w={'full'} alignItems={'start'}>
              <PoolKeyValue
                k={'Available'}
                v={`${(pool.available / 1_000_000).toFixed(1)} M USDC`}
              />
              <PoolKeyValue k={'APY'} v={`${pool.apy}%`} />
              <PoolKeyValue k={'Term'} v={pool.term} />
              <PoolKeyValue k={'Withdrawal'} v={pool.withdrawal} />
              <PoolKeyValue k={'Annual CO₂ savings'} v={pool.co2} />
              <PoolKeyValue k={'Solar, wind'} v={pool.watt} />
            </VStack>
            <Button
              textTransform={'uppercase'}
              h={'40px'}
              px={'40px'}
              fontSize={14}
              bg={'blackAlpha.500'}
              _hover={{ bg: 'blackAlpha.400' }}
            >
              Deposit
            </Button>
          </VStack>
        </VStack>
      </Container>
    )
  }

  const Pools = () => (
    <Flex
      display={swiper.current ? 'flex' : 'none'}
      w='full'
      h={`${POOL_CARD_HEIGHT}px`}
      pos={'absolute'}
      transform={`translateY(${isMobile ? '50' : '60'}px)`}
    >
      <Swiper
        ref={swiper}
        slidesPerView={document.documentElement.clientWidth / (280 + (isMobile ? 15 : 24))}
        spaceBetween={isMobile ? 15 : 24}
        slidesOffsetBefore={offsetWidth}
        slidesOffsetAfter={offsetWidth}
        style={{
          width: '100%',
          height: '100%',
        }}
      >
        {pools.map((pool, i) => (
          <SwiperSlide key={i}>
            <Pool pool={pool} />
          </SwiperSlide>
        ))}
      </Swiper>
    </Flex>
  )

  return (
    <VStack
      w={'full'}
      h={`calc(30px + 36px + ${POOL_CARD_HEIGHT}px)`}
      spacing={{ sm: '16px', md: '36px' }}
      alignItems={{ sm: 'start', md: 'center' }}
    >
      <Header />
      <Pools />
    </VStack>
  )
}
