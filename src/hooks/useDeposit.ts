import { useTranchePoolInvestJunior } from '@/contracts/generated'
import { useChain } from '@/hooks'
import { Pool } from '@/types'
import { Logger } from '@/utils'
import { TransactionReceipt } from 'viem'
import { useWaitForTransaction } from 'wagmi'
import { WriteContractResult } from 'wagmi/dist/actions'

interface IUseDeposit {
  pool?: Pool
  amount: bigint
  onSign?: (tx: WriteContractResult) => void
  onConfirm?: (receipt: TransactionReceipt) => void
}

export const useDeposit = ({ pool, amount, onSign, onConfirm }: IUseDeposit) => {
  const logger = new Logger(useDeposit.name)

  const { chain } = useChain()

  const {
    data: tx,
    isLoading: isLoadingSign,
    write: mutate,
  } = useTranchePoolInvestJunior({
    address: pool?.address[chain.id],
    args: [amount],
    onSuccess: (tx) => {
      logger.success('TX_SIGNED', pool, amount, tx.hash)
      onSign?.(tx)
    },
  })

  const {
    data: receipt,
    isLoading: isLoadingConfirm,
    isSuccess,
  } = useWaitForTransaction({
    hash: tx?.hash,
    onSuccess: (receipt) => {
      logger.success('TX_CONFIRMED', receipt)
      onConfirm?.(receipt)
    },
  })

  return {
    mutate,
    data: { tx, receipt },
    isLoadingSign,
    isLoadingConfirm,
    isLoading: isLoadingSign || isLoadingConfirm,
    isSuccess,
  } as const
}
