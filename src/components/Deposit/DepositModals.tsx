import { DepositModal, DepositSubmittedModal } from '@/components'

export const DepositModals = () => (
  <>
    <DepositModal />
    <DepositSubmittedModal />
  </>
)
