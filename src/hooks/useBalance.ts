import { MOCK_ERC20 } from '@/constants'
import { useChain } from '@/hooks'
import { Token } from '@/types'
import { useAccount, useQuery, useBalance as useWagmiBalance } from 'wagmi'

interface IUseBalance {
  token?: Token
  refetchInterval?: number | false
}

export const useBalance = (props?: IUseBalance) => {
  const { address } = useAccount()
  const { chain } = useChain()
  const { token = MOCK_ERC20, refetchInterval } = props ?? {}

  const query = useWagmiBalance({
    address,
    token: token?.address[chain.id],
    formatUnits: token?.decimals,
  })

  useQuery(['balance-interval-query'], { queryFn: query.refetch, refetchInterval })

  return query
}
