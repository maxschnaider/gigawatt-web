import { Button, ConnectButton } from '@/components'
import { useDepositStore } from '@/stores'
import { DepositUIState } from '@/types'

export const DepositButton = () => {
  const { openDepositModal, state } = useDepositStore()

  return state === DepositUIState.WalletNotConnected ? (
    <ConnectButton w={'full'} />
  ) : (
    <Button onClick={openDepositModal}>Deposit</Button>
  )
}
