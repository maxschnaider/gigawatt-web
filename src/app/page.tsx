'use client'

import { MainPagePools, MainPageStats, MainLayout } from '@/components'
import { Providers } from '@/providers'

const MainPage = () => (
  <Providers>
    <MainLayout>
      <MainPageStats />
      <MainPagePools />
    </MainLayout>
  </Providers>
)

export default MainPage
