'use client'

import { MainLayout } from '@/components'
import { Providers } from '@/providers'

const Layout = ({ children }: React.PropsWithChildren) => (
  <Providers>
    <MainLayout gap={'60px'}>{children}</MainLayout>
  </Providers>
)

export default Layout
