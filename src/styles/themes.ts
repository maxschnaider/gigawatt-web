import { extendTheme as ChakraTheme } from '@chakra-ui/react'
import { darkTheme, lightTheme, Theme as RainbowKitTheme } from '@rainbow-me/rainbowkit'

const fonts = `Verdana, sans-serif`
const colors = {
  brand: '#ffdf59',
  text: { gray: '#7C7E81' },
  border: { gray: '#B8B8B8' },
  bg: { gray: '#F3F3F3' },
}

export const chakraTheme = ChakraTheme({
  initialColorMode: 'light',
  useSystemColorMode: false,
  fonts: {
    heading: fonts,
    body: fonts,
  },
  colors,
  // components: {
  //   Heading: {
  //     baseStyle: {
  //       color: colors.font,
  //     },
  //   },
  // },
  // styles: {
  //   global: {
  //     body: {
  //       bg: 'white',
  //       color: colors.font,
  //     },
  // a: {
  //   _hover: {
  //     color: 'blue',
  //     textDecoration: 'none !important',
  //   },
  // },
  // },
  // },
  breakpoints: {
    sm: '320px',
    md: '768px',
    lg: '1000px',
    xl: '1200px',
    xxl: '1500px',
  },
})

const rainbowKitDefaultTheme = lightTheme({
  borderRadius: 'medium',
})
export const rainbowKitTheme: RainbowKitTheme = {
  ...rainbowKitDefaultTheme,
  fonts: {
    body: fonts,
  },
  colors: {
    ...rainbowKitDefaultTheme.colors,
    accentColor: 'gray',
    // modalBackground: 'black',
  },
}
