export class NumberUtil {
  static formatThousands = (v?: number | string) =>
    v?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')

  static toFixed = (v?: number | string, decimals?: number) => Number(v ?? 0).toFixed(decimals)
}
