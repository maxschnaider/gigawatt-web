import { Container } from '@/components'
import { NumberUtil } from '@/utils'
import { Flex, Heading, Text } from '@chakra-ui/react'
import {
  CartesianGrid,
  Line,
  LineChart,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'

const mockChartData = [
  {
    month: 'Jan',
    Watt: 325_000,
  },
  {
    month: 'Feb',
    Watt: 269_000,
  },
  {
    month: 'Mar',
    Watt: 420_000,
  },
  {
    month: 'Apr',
    Watt: 620_000,
  },
  {
    month: 'May',
    Watt: 500_000,
  },
  {
    month: 'Jun',
    Watt: 710_000,
  },
  {
    month: 'Jul',
    Watt: null,
  },
  {
    month: 'Aug',
    Watt: null,
  },
  {
    month: 'Sep',
    Watt: null,
  },
  {
    month: 'Oct',
    Watt: null,
  },
  {
    month: 'Nov',
    Watt: null,
  },
  {
    month: 'Dec',
    Watt: null,
  },
]

export const MainPageStatsChart = () => (
  <Flex flexBasis={'69%'} flexDir={'column'} gap={4} alignItems={'center'}>
    <Text
      textTransform={'uppercase'}
      fontSize={{ sm: 11, md: 14 }}
      display={{ sm: 'none', md: 'block' }}
    >
      Watts in protocol
    </Text>
    <Container w={'full'} h={'220px'} flexDir={'column'} gap={5}>
      <Heading fontSize={{ sm: 18, md: 24 }} p={{ sm: '6px 8px 0', md: 0 }}>{`710,000 W `}</Heading>
      <ResponsiveContainer width='100%' height='100%'>
        <LineChart
          data={mockChartData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid vertical={false} />
          <XAxis
            dataKey='month'
            axisLine={false}
            tickLine={false}
            padding={{ left: 20, right: 20 }}
            fontSize={11}
            opacity={0.69}
          />
          <YAxis
            axisLine={false}
            tickLine={false}
            fontSize={11}
            opacity={0.69}
            width={20}
            tickFormatter={(value) => `${value / 1000}k`}
          />
          <Tooltip
            formatter={(value) => NumberUtil.formatThousands(Number(value))}
            contentStyle={{ borderRadius: '12px' }}
          />
          <Line type='monotone' dataKey='Watt' stroke='black' strokeWidth={2} dot={false} />
        </LineChart>
      </ResponsiveContainer>
    </Container>
  </Flex>
)
