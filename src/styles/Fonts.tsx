import { Global } from '@emotion/react'

export const Fonts = () => (
  <Global
    styles={[
      {
        '@font-face': {
          fontFamily: '',
          src: 'url("/assets/fonts/.ttf") format("truetype")',
        },
      },
    ]}
  />
)
