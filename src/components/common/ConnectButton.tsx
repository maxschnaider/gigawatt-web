import { Button, ButtonProps } from '@/components'
import { ConnectButton as RainbowKitConnectButton } from '@rainbow-me/rainbowkit'

export const ConnectButton = ({ ...props }: ButtonProps) => {
  return (
    <RainbowKitConnectButton.Custom>
      {({ account, chain, openAccountModal, openChainModal, openConnectModal, mounted }) => {
        const ready = mounted
        const connected = ready && account && chain
        return (
          <div
            {...(!ready && {
              'aria-hidden': true,
              style: {
                opacity: 0,
                pointerEvents: 'none',
                userSelect: 'none',
              },
            })}
          >
            {(() => {
              if (!connected) {
                return (
                  <Button px={'25px'} onClick={openConnectModal} {...props}>
                    Connect Wallet
                  </Button>
                )
              }
              if (chain.unsupported) {
                return (
                  <Button px={'25px'} onClick={openChainModal} {...props}>
                    Wrong Network
                  </Button>
                )
              }
              return (
                <Button px={'25px'} onClick={openAccountModal} {...props}>
                  {account.displayName}
                </Button>
                // <div style={{ display: 'flex', gap: 12 }}>
                //   <button
                //     onClick={openChainModal}
                //     style={{ display: 'flex', alignItems: 'center' }}
                //     type='button'
                //   >
                //     {chain.hasIcon && (
                //       <div
                //         style={{
                //           background: chain.iconBackground,
                //           width: 12,
                //           height: 12,
                //           borderRadius: 999,
                //           overflow: 'hidden',
                //           marginRight: 4,
                //         }}
                //       >
                //         {chain.iconUrl && (
                //           <img
                //             alt={chain.name ?? 'Chain icon'}
                //             src={chain.iconUrl}
                //             style={{ width: 12, height: 12 }}
                //           />
                //         )}
                //       </div>
                //     )}
                //     {chain.name}
                //   </button>
                //   <button onClick={openAccountModal} type='button'>
                //     {account.displayName}
                //     {account.displayBalance ? ` (${account.displayBalance})` : ''}
                //   </button>
                // </div>
              )
            })()}
          </div>
        )
      }}
    </RainbowKitConnectButton.Custom>
  )
}
